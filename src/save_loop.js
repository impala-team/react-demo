
import 'medium-editor/dist/css/medium-editor.css';
import 'medium-editor/dist/css/themes/default.css';

import impala from './imgs/impala-logo.png';

import React from "react";
import axios from 'axios';

// import { w3cwebsocket as W3CWebSocket } from "websocket";

import {VictoryScatter,   VictoryGroup,  } from "victory";
import ClipLoader from "react-spinners/ClipLoader";
import S3 from 'react-aws-s3';

// axios.defaults.headers.post['Access-Control-Allow-Origin'] = '*';

//const url_video =  'https://www.impala.best:80'
//const url_video =  'http://localhost:5000'
const url_video = 'https://23gz53p7r5.execute-api.eu-west-1.amazonaws.com/prod'


// PID used to generate indivudual results --541000
const DEMO_PID = 'zofjebvvck'
const back_image =  './react-demo/imgs/back.png'
const contact_id = 1752;
const BUCKET = 'impala-video-cache'

 const AWS_SECRET_ACCESS_KEY  = process.env.REACT_APP_AWS_SECRET_ACCESS_KEY;
const  AWS_ACCESS_KEY_ID  = process.env.REACT_APP_AWS_ACCESS_KEY_ID;
console.log('aws key is good: ', AWS_ACCESS_KEY_ID!=="")
const config = {
    bucketName: BUCKET,
    dirName: 'saving_aws', /* optional */
    region: 'eu-west-1',
    accessKeyId: AWS_ACCESS_KEY_ID,
    secretAccessKey: AWS_SECRET_ACCESS_KEY,
  /*  s3Url: 'https:/your-custom-s3-url.com/',  optional */
}
 
const ReactS3Client = new S3(config);

const make_xy = (xx) =>({x: xx%4, y: Math.floor(xx/4) })


const ball_list = [
{ball: 12, type:"calibrate", delay:5, id: 0},
{ball: 13, type:"calibrate", delay:5, id: 1},
{ball: 14, type:"calibrate", delay:5, id: 2},
{ball: 15, type:"calibrate", delay:5, id: 3},
{ball: 8, type:"calibrate", delay:5, id: 4},
{ball: 9, type:"calibrate", delay:5, id: 5},
{ball: 10, type:"calibrate", delay:5, id: 6},
{ball: 11, type:"calibrate", delay:5, id: 7},
{ball: 4, type:"calibrate", delay:5, id: 8},
{ball: 5, type:"calibrate", delay:5, id: 9},
{ball: 6, type:"calibrate", delay:5, id: 10},
{ball: 7, type:"calibrate", delay:5, id: 11},
]

var rand_token = function() {
    return Math.random().toString(36).substr(2); // remove `0.`
};
// const client = new W3CWebSocket( 'wss://www.impala.best:80');
// const client = new W3CWebSocket( 'wss://li1292-137.members.linode.com:80');





export  class ImpalaLoop extends React.Component {
constructor(props){
  super(props);

    localStorage.setItem('starter', props.starter );
this.disable=false;
this.preview = React.createRef();
  this.recorder = null;
  this.piece_data = [];


  this.state={do_disable:false, scene_list:ball_list, 
    is_closed : false, is_after_all:false,
    after_sending: false,
     stopper: 0, goodsend: 0}
  this.stream = null;


  } 

start_scenario(stream){
	var old_state = this.state;
	old_state.do_disable = true
   this.setState(old_state)
 this.slideshow_step(this.state.scene_list, stream)




localStorage.setItem('user_id', rand_token())
}


 background_image(){
   var starter  =parseInt( localStorage.getItem('starter') || "-1" ,10  );
   var len_scene = this.state.scene_list.length;

   if (starter<ball_list.length){
    return back_image;  
  } else  if (starter<len_scene && this.state.scene_list[starter].type==='photo' ){
    console.log('img', this.state.scene_list[starter].img)
    return  this.state.scene_list[starter].img.src
  }
    return back_image;  

 }


  slideshow_step(scene_list, stream){


    var starter = parseInt(localStorage.getItem('starter', -1), 10);
    var starterpp= starter+1
      localStorage.setItem('starter', starterpp );
        // console.log('set step',starterpp+1+" ,  "+
        //     this.state.scene_list.length)
    if (starterpp>this.state.scene_list.length){

           // console.log('closed  buffering: ',client.bufferedAmount);
    // var first_int = localStorage.getItem("first_interval" );
    // clearTimeout(first_int);
           console.log('last console');

      // return
    } 
else if (starterpp===this.state.scene_list.length){
      var old_state = this.state;
  old_state.is_after_all = true
   this.setState(old_state)
console.log('inside last:  ', this.state.is_after_all);
}
    

    // console.log('starting sth: ', starter)



    if (starter>=0){
    this.stopRecords(starter)
  }
    this.startRecording(stream)
            if (starterpp<this.state.scene_list.length){
              this.move_slide(this.state.scene_list, starterpp, stream);
            }

          this.forceUpdate();

      
      


  }
  move_slide(scene_list, starterpp, stream){
    setTimeout((scene_list)=>{
      this.slideshow_step(scene_list, stream)
    },
      scene_list[starterpp].delay * 1000)
  }

set_slides_api(data_imgs){
  data_imgs.data.forEach((picture) => {
        var img = new Image();
        img.src = picture.imgsrc;
        picture.img = img;
    });
console.log(data_imgs.data)

	var new_scene = this.state.scene_list.concat(data_imgs.data || [] )
	var new_state = this.state ;
	new_state.scene_list = new_scene


	        this.setState(new_state);

}




componentDidMount(){


// client.onmessage = function(data){
//   console.log('gotten: ', data)
// }


//    client.onopen = function() {
                  
//                   // Web Socket is connected, send data using send()
//                   console.log("Web Socket is connected !!");
//                };

	// var scene_list = scene_list.concat(images_stimuli)

	var params = (new URL(document.location)).searchParams;
	var json_uri = params.get('jsonbin_id') || "5e56ea4a61ef782ce2bff742"
  var url_req = 'https://api.jsonbin.io/b/'+json_uri
  

  var pid_uri = params.get('pid') || null;

  if (pid_uri !== null){
    url_req = url_video + '/stimuli?pid=' + pid_uri

  }

	console.log(pid_uri,url_req)


const options = {
  method: 'GET',
  url:url_req
};
axios(options).then(this.set_slides_api.bind(this))



    localStorage.setItem('contact_id', contact_id);








  }


ball_state(){
	  	var starter = parseInt(localStorage.getItem('starter', -1), 10);
	  	if (starter>=0 && starter<this.state.scene_list.length
	  	 &&this.state.scene_list[starter].type==="calibrate"){
	  		return [make_xy(this.state.scene_list[starter].ball)]

	  	}
	  	return []
}




logInUser(){
    // this.preview.captureStream = this.preview.captureStream || this.preview.mozCaptureStream;
    navigator.mediaDevices.getUserMedia({
    video: true,
    audio: false
  }).then(stream => {

        this.start_scenario(stream)

  })
    .catch(function(err){
    console.log('error', err)

  }
    );

}

 startRecording(stream ) {
 this.recorder = new MediaRecorder(stream);
  this.recorder.ondataavailable = this.process_piece.bind(this)

  this.recorder.start(); 
}


  stopRecords(){


    if (this.recorder.state === "recording"){
 this.recorder.stop()
}



  }



    process_piece(event) {
    let recordedBlob = new Blob([event.data], { type: "video/webm" });


    // console.log("Successfully recorded " + recordedBlob.size + " bytes of " +
    //     recordedBlob.type + " media.");

    var starter = parseInt( localStorage.getItem('starter', '-1'), 10)
    // console.log('inside starter', starter)

    var scene_len = this.state.scene_list.length;
    var stimul_id = this.state.scene_list[(starter-1)<scene_len ?  (starter-1) : 0 ].id;
    var user_id = localStorage.getItem('user_id', 'test_subject')


 // console.log(user_id, "/"+rand_token())
ReactS3Client
    .uploadFile(recordedBlob, user_id+"/stimul_"+stimul_id)
    .then(this.succSent.bind(this))
    .catch(this.failSent.bind(this));
 
/*

     const data = new FormData();
  data.set('file', recordedBlob );
  data.set('stimul_id', stimul_id);
  data.set('user_id', user_id);


axios({
    method: 'post',
    url: url_video     +'/video?stimul_id='+stimul_id,
    data: data,
    headers: {'Content-Type': 'multipart/form-data' }
    })
    .then(this.succSent.bind(this))
    .catch(this.failSent.bind(this));
*/
  }

    failSent(response) {
        	var old_state = this.state;
	        old_state.stopper += 1
           this.setState(old_state);
        console.log("stopper: ", response);
    var scene_length = this.state.scene_list.length;
//        //handle success
        if (this.state.stopper===scene_length){
            this.sendProcessRequest()

      
        }
    }

    succSent(response) {
            	var old_state = this.state;
	        old_state.stopper += 1;
          old_state.goodsend += 1;
           this.setState(old_state);

    var scene_length = this.state.scene_list.length;
//        //handle success
        if (this.state.stopper===scene_length){
          this.sendProcessRequest()

        }      
       

//        console.log('good stuff',starter+" "+scene_length)
    }
    sendProcessResponse(response){
      console.log(response)
     var old_state = this.state;
          old_state.is_after_all = false;
          old_state.after_sending = true;

           this.setState(old_state);
    }
    sendProcessRequest(){

    var user_id = localStorage.getItem('user_id', 'test_subject');
      var params = (new URL(document.location)).searchParams;

  var pid = params.get('pid') || DEMO_PID;

axios({
    method: 'post',
    url: url_video+  '/result?subject_id='+user_id+'&pid=' + pid
    }).then(this.sendProcessResponse.bind(this))
    .catch(this.sendProcessResponse.bind(this));

    }
  componentWillUnmount(){
  }
  render() {
      return (

<div className='fluidCont'
style={{backgroundImage: 'url('+ this.background_image()+")" }}

 >
 <header className='App-header'  style={{ position: 'absolute', zIndex: '2' }}   >

  <ClipLoader
        style={{marginLeft:0}}
          size={50}
          color={"#fff"}
          loading={this.state.is_after_all }
        />




{
  this.state.after_sending ?
<h1 className="final-text">
Finished transfer { this.state.goodsend} stimuli.

</h1>

: null

}



{ this.state.do_disable  ? null :
	<div className ="start-prompt" >

	        <img src={impala} height={80} width={200}  alt="logo" />

          <button type="button"    onClick={() => this.logInUser()} 
          className="btn btn-primary account__btn">Start Sequence</button>
          
           

          </div>
          }
            

</header>
        





<VictoryGroup   style={{ position: 'absolute', zIndex: '1' , left:'0', top: '0'}}
    maxDomain={{x:3,y:4}}
  minDomain={{x:0,y:0}}

className="svg-draw" width={1600} height={800}>

      
  
  <VictoryScatter 

  standalone={false}
className="svg-balls"
  data={this.ball_state() } 
  size={22}
      style={{ data: {fill:    ({ datum }) => "#5c42ba" }  }}
   >
   </VictoryScatter>


</VictoryGroup>


</div>

);}

}
